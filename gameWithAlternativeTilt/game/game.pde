//import net.silentlycrashing.gestures.*;
//import net.silentlycrashing.gestures.preset.*;
//import net.silentlycrashing.util.*;

float depth = 2000;

//GestureAnalyzer brain;
//ConcurrentGestureListener CCZREar;

float planeEdge = 1000;
float radiusSphere = 80;

SphereMover mover;


void setup() {
  size(500, 500, P3D);
//  noStroke();
//  brain = new MouseGestureAnalyzer(this);
//  brain.setVerbose(true);
//  CCZREar = new ConcurrentCCWTwirlListener(this, brain);
//  CCZREar.registerOnAction("mouseDragged", this);
  mover = new SphereMover();
}

float rX = 0;
float rZ = 0;

float rY = 0;

float speedFactor = 1;

void draw() {
  camera(width/2, height/4, depth, 250, 250, 0, 0, 1, 0);
  //directionalLight(50, 100, 125, 0, -1, 0);
  //ambientLight(102, 102, 102);
  background(200);
  translate(width/2, height/2, 0);
  //float rz = map(mouseY, 0, height, 0, PI);
  //float ry = map(mouseX, 0, width, 0, PI);
  rotateZ(rZ);
  rotateX(rX);
  rotateY(rY);
  box(1000, 40, 1000);
  translate(0, -(20 + radiusSphere), 0);
  mover.checkEdges();
  mover.update();
  mover.display();
}

void mouseDragged() {
  float rXint = speedFactor*(map(mouseY, (0), (height), (PI/3), (-PI/3)));
  if (rXint >= 0) {
    rX = min(rXint, (PI/3));
  } else {
    rX = max(rXint, (-PI/3));
  }
  float rZint = speedFactor*(map(mouseX, (0), (width), (-PI/3), (PI/3)));
  if (rZint >= 0) {
    rZ = min(rZint, (PI/3));
  } else {
    rZ = max(rZint, (-PI/3));
  }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == LEFT) {
      rY = rY - ((PI/12) * abs(speedFactor));
    } else if (keyCode == RIGHT) {
      rY = rY + ((PI/12) * abs(speedFactor));
    }
  }
}

void mouseWheel(MouseEvent event) {
  float e = event.getCount();
  speedFactor = min(speedFactor * map(e, 100, -100, 0.00001, 2), 50);
  println(e + "   " + speedFactor);
}

