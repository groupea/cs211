class SphereMover {
  
  PVector location;
  PVector velocity;
  PVector gravityForce = new PVector(0, 0, 0);
  
  float gravityConstant = 3.5;
  float deltaTime = (1.0/20.0);
  
  float normalForce = 1;
  float mu = 0.01;
  float frictionMagnitude = normalForce * mu;
  
  float elasticity = 0.60;

  SphereMover() {
    location = new PVector(0, 0, 0);
    velocity = new PVector(0, 0, 0);
  }

  void update() {
    gravityForce.x = sin(rZ) * gravityConstant;
    gravityForce.z = -sin(rX) * gravityConstant;
    System.out.println("Gravity: " + gravityForce);
    PVector friction = velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    friction.mult(deltaTime);
    System.out.println("Friction: " + friction);
    velocity.add(friction);
    gravityForce.mult(deltaTime);
    velocity.add(gravityForce);
    System.out.println("Velocity: " + velocity);
    location.add(velocity);
  }
  
  void display() {
    //stroke(0);
    //strokeWeight(2);
    //fill(127);
    translate(location.x, location.y, location.z);
    sphere(radiusSphere);
  }

  void checkEdges() {
    if (location.x > (planeEdge / 2)) {
      location.x = (planeEdge / 2);
      velocity.x = elasticity*(velocity.x * (-1));
    } else if (location.x < -(planeEdge / 2)) {
      location.x = -(planeEdge / 2);
      velocity.x = elasticity*(velocity.x * (-1));
    }
    if (location.z > (planeEdge / 2)) {
      location.z = (planeEdge / 2);
      velocity.z = elasticity*(velocity.z * (-1));
    } else if (location.z < -(planeEdge / 2)) {
      location.z = -(planeEdge / 2);
      velocity.z = elasticity*(velocity.z * (-1));
    }
  }
}

