package game;

import processing.core.*;

//Cylinder to put on the plane

@SuppressWarnings("serial")
class Cylinder extends PApplet{
  //Cylinder parameters
  float cylinderBaseSize = Game.cylinderBaseSizeInGame;
  float cylinderHeight = Game.cylinderHeightInGame;
  int cylinderResolution = Game.cylinderResolutionInGame;
  PShape tree;
  PShape top = new PShape();
  PShape cap = new PShape();
  //Cylinder position
  float x,y;



  
  //Basic constructer
  Cylinder(float x, float y){
    this.x=x;
    this.y=y;
  }
  
  //Method to put cylinder in place
  public void setup(){
    tree = loadShape("arbre.obj");
  }
  
  //Method to draw the full cylinder wothout interfering the system of coordinates before and after
    void display(){
    pushMatrix();
    translate(x,y,20);
    scale(20);
    rotateX(PI/2);
    shape(tree);
    popMatrix();
  } 
}
