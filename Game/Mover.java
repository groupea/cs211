//The ball and all the forces acting on it:
//- Gravity
//- Friction
//Also calculates the collisions with cylinders and the boarders
package game;

import processing.core.*;

@SuppressWarnings("serial")
class Mover extends PApplet{
  PVector location;
  PVector velocity;
  PVector gravityForce; 
  
  //Constants for calculation of the velocity changes
  float gravityConstant = 0.5f;
  float elasticity = 0.60f;
  float normalForce = 1;
  float mu = 0.01f;
  float frictionMagnitude = normalForce * mu;

  //Default positions and directions
  Mover() {
    location = new PVector(0, 0, 0);
    velocity = new PVector(0, 0, 0);
    gravityForce = new PVector(0, 0, 0);
  }
  
  //Re-evaluating the force vectors and directions and applying the
  //effect on the velocity and calculating new position of the ball
  
  void update() {
    //New gravity depends on the rotation of the plane
    gravityForce.x = (float) (Math.sin(Game.rZ) * gravityConstant);
    gravityForce.z = (float) (-Math.sin(Game.rX) * gravityConstant);
    //Friction always has the same magnitude and is in opposite direction of the velocity
    PVector friction = velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    //At this stage gravityForce represents the overall force acting on the ball
    //so we add friction to it and get the total force which may directly used as
    //accelaration as we don't consider mass of the ball. Moreover we even asssume
    //that delta time is equal to 1 so in our case force is change in velocity
    gravityForce.add(friction);
    velocity.add(gravityForce);
    //New position is calculated by applying the velocity, since we assume delta time
    //equal zero => we just add the velocity vector to the position vector
    location.add(velocity);
  }
  
  //Method to display the ball without intervining the system of coordinates before and after
  
  void display() {
    pushMatrix();
    translate(location.x, location.y, location.z);
    sphere(Game.radius);
    popMatrix();
  }
  
  //Method for cheking if the ball went over the edge on the plane and put it back in
  
  void checkEdges() {
    //Right Edge
    if ((location.x + Game.radius) > (Game.boxEdge/2)) {
      velocity.x = -(velocity.x * elasticity);
      location.x = (Game.boxEdge/2) - Game.radius;
      Game.scoreBoard.addScore(-velocity.mag());
    } else if ((location.x - Game.radius) < -(Game.boxEdge/2)) {
      //Left Edge
      velocity.x = -(velocity.x * elasticity);
      location.x = -(Game.boxEdge/2) + Game.radius;
      Game.scoreBoard.addScore(-velocity.mag());
    }
    //Far Edge
    if ((location.z + Game.radius) > (Game.boxEdge/2)) {
      velocity.z = -(velocity.z * elasticity);
      location.z = (Game.boxEdge/2) - Game.radius;
      Game.scoreBoard.addScore(-velocity.mag());
    } else if ((location.z - Game.radius) < -(Game.boxEdge/2)) {
      //Close Edge
      velocity.z = -(velocity.z * elasticity);
      location.z = -(Game.boxEdge/2) + Game.radius;
      Game.scoreBoard.addScore(-velocity.mag());
    }
  }
  
  //Check for the collision with the cylinders and put the ball at right place
  
  void checkCylinderCollision(){
    //Need to check every cylinder
    for (Cylinder c: Game.adder.cylinders ){
      //z coordinate of the ball corresponds to the y coordinate of the culinder
      //Take into cinsideration of the Game.radius of the ball, and the Game.radius of the base of the cylinder
      //Need to check each separately because collision may occure in multiple directions at the same time
      //Collision of the ball with the closer side of the cylinder
      if (((location.z + Game.radius) > (c.y - c.cylinderBaseSize)) &&
          !(location.z > c.y) &&
          (location.x < (c.x + c.cylinderBaseSize)) &&
          (location.x > (c.x - c.cylinderBaseSize))) {
            velocity.z = -(velocity.z * elasticity);
            location.z = (c.y - c.cylinderBaseSize - Game.radius);
            Game.scoreBoard.addScore(velocity.mag());
      }
      //Collision of the ball with the further side of the cylinder
      if (((location.z - Game.radius) < (c.y + c.cylinderBaseSize)) &&
          !(location.z < c.y) &&
          (location.x < (c.x + c.cylinderBaseSize)) &&
          (location.x > (c.x - c.cylinderBaseSize))) {
            velocity.z = -(velocity.z * elasticity);
            location.z = (c.y + c.cylinderBaseSize + Game.radius);
            Game.scoreBoard.addScore(velocity.mag());
      }
      //Collision of the ball with the left side of the cylinder
      if (((location.x + Game.radius) > (c.x - c.cylinderBaseSize)) &&
          !(location.x > c.x) &&
          (location.z < (c.y + c.cylinderBaseSize)) &&
          (location.z > (c.y - c.cylinderBaseSize))) {
            velocity.x = -(velocity.x * elasticity);
            location.x = (c.x - c.cylinderBaseSize - Game.radius);
            Game.scoreBoard.addScore(velocity.mag());
      }
      //Collision of the ball with the right side of the culinder
      if (((location.x - Game.radius) < (c.x + c.cylinderBaseSize)) &&
          !(location.x < c.x) &&
          (location.z < (c.y + c.cylinderBaseSize)) &&
          (location.z > (c.y - c.cylinderBaseSize))) {
            velocity.x = -(velocity.x * elasticity);
            location.x = (c.x + c.cylinderBaseSize + Game.radius);
            Game.scoreBoard.addScore(velocity.mag());
      }
      
      //Check that ball is not partly inside the cylinder once collided
      if(new PVector(location.x, location.z).dist(new PVector(c.x, c.y)) <= (Game.radius + c.cylinderBaseSize)){
        //Manage location
        PVector n = new PVector(location.x, 0, location.z);
        n.sub(c.x, 0, c.y);
        n.normalize();
        PVector n2 = n.get();
        n2.mult(Game.radius + c.cylinderBaseSize);
        n2.add(c.x, 0, c.y);
        location = n2;
        //Manage velocity
        float x = (2 * velocity.dot(n));
        n.mult(x);
        velocity.sub(n);
        velocity.mult(elasticity);
        Game.scoreBoard.addScore(velocity.mag());
      }
    }
  }
}
