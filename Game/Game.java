package game;

import processing.core.*;
import processing.event.MouseEvent;

@SuppressWarnings("serial")
public class Game extends PApplet{

static Mover mover;
static Adder adder;
static Cylinder model;
static ScoreBoard scoreBoard;


//General Initial Settings of the Game

final static float depth = 2000;
final static int radius = 50;    //Radius of the ball
static float rX = 0;      //Rotation angles of the plane
static float rZ = 0;
static float rY = 0;
static float speedFactor = 1;      //Spped control
final static float boxEdge = 1000;        //Length of the Edge of the plane 
boolean stop = false;      //Flag for buidling mode

//Cylinder Settings for the Game

final static float cylinderBaseSizeInGame = 50;
final static float cylinderHeightInGame = 100;
final static int cylinderResolutionInGame = 40;


public void setup() {
  size(1000, 1000, P3D);
  mover = new Mover();
  model = new Cylinder(0,0);
  adder = new Adder();
  scoreBoard = new ScoreBoard(createGraphics(width, (height/8), P2D));

}

public void draw() {
  camera(width/2, height/2, depth, width/2, height/2, 0, 0, 1, 0);
  background(200);
  translate(width/2, height/2, 0);
  lights();

  
 
  
  //Normal routine
  
  if (!stop){
   scoreBoard.display();
    Hough hough = new Hough();
    PVector v = hough.compute();
    rotateZ(rZ);
    rotateX(v.x);
    rotateY(v.y);
    box(boxEdge, 40, boxEdge);
   
  //The part that draws Cylinders
   
    pushMatrix();
    rotateX(PI/2);
    translate(0, 0 , 20);
    adder.display();
    popMatrix();
    
  //End of the part that draws Cylinders
  
  //Part that draws the ball
  
    translate(0, -(radius + 20), 0);
    mover.checkEdges();
    mover.checkCylinderCollision();
    mover.update();
    mover.display();
    
  //End of the part that draws the ball
  
  } else {
    
  //Building Mode Routine
    
    //Bring the plane up the camera to avoid perspective
    translate(0, 0, (int)(depth - ((height/2.0) / tan(PI*30.f / 180.f))) - 20);
    //Draw plane and cylinders
    box(boxEdge, boxEdge, 40);
    adder.display();
    pushMatrix();
    //Using the fact that plane is up close, it is the one to one relation except for signs
    //Hence we use mapping to correct the signs and display the cylinder where it would be placed
    translate(map(mouseX, 0, width, -(width/2), (width/2)), map(mouseY, 0, height, -(height/2), (height/2)), 0);
    model.setup();
    model.display();
    popMatrix();
    //Going back to the plane system of coordinates and draw the ball as usual
    pushMatrix();
    translate(mover.location.x, mover.location.z, (radius - 20));
    sphere(radius);
    popMatrix();
  }
  
}


public void mouseDragged() {
  //Tools outside of the Building Mode
  if (!stop&&mouseY<7*height/8){
    //Rotating the plane around X axis ensuring the boundaries of abs(andle) < 30 degrees (considering speedFactor)
    float rXint = speedFactor*(map(mouseY, (0), (height), (PI/3), (-PI/3)));
    if (rXint >= 0) {
      rX = min(rXint, (PI/3));
    } else {
      rX = max(rXint, (-PI/3));
    }
    //Rotating the plane around Z axis ensuring the boundaries of abs(andle) < 30 degrees (considering speedFactor)
    float rZint = speedFactor*(map(mouseX, (0), (width), (-PI/3), (PI/3)));
    if (rZint >= 0) {
      rZ = min(rZint, (PI/3));
    } else {
      rZ = max(rZint, (-PI/3));
    }
  }
}

public void keyPressed() {
  if (key == CODED) {
    //Rotating the plane aroundY axis without any boundaries (considering sppedFactor)
    if (keyCode == LEFT) {
      rY = rY - ((PI/12) * abs(speedFactor));
    } else if (keyCode == RIGHT) {
      rY = rY + ((PI/12) * abs(speedFactor));
    }
    //Setting the Building Moode
    if (keyCode == SHIFT) {
      stop = true;
    }
  }
}

public void mousePressed(){ 
 
  //Once in Building Mode: the tool to put in place the cylinders
  //Since mouse axises are only positive we need to remap them to
  //allow negative values that we may directly use for cylinder
  //setting as the relation is one to one since we brought the
  //plane up to the camera when we entered the building mode.
 
  if(stop){
      // We make sure that we can put cylinders on the plane iff they will be fully on it
      float cylX = map(mouseX, 0, width, -(width/2), (width/2));    //x coordinate of the future cylinder
      float cylY = map(mouseY, 0, height, -(height/2), (height/2));  //y coordinate of the future cylinder
      float allowedDist = (boxEdge/2 - (cylinderBaseSizeInGame)); //distance from the center ensuring the condition
      println("cylX " + cylX + " , cylY " + cylY + " allowedDist " + allowedDist);
      if((cylX < allowedDist) && (cylX > -(allowedDist)) &&
        (cylY < allowedDist) && (cylY > -(allowedDist))){
          adder.add(cylX, cylY);
       }
  } 
}

public void keyReleased(){
  
  //Once SHIFT is released we exit the Building Mode
  
  if (keyCode == SHIFT){
   stop = false; 
  }
}

public void mouseWheel(MouseEvent event) {
  
  //Using the mouse wheel we may adjust the speed of the rotations by changing the speedFactor
  
  float e = event.getCount();
  speedFactor = min(speedFactor * map(e, 100, -100, 0.00001f, 2), 50);
}
}


