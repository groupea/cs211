package game;

import java.util.*;
import processing.core.*;

@SuppressWarnings("serial")
class ScoreBoard extends PApplet{
   
  //Principal background
   PGraphics g;
   PGraphics topView;
   PGraphics scoreboard;
   PGraphics barChart;
   PGraphics scrollbar;
   
   HScrollBar bar;
   
   LinkedList<Pair> list;
   ArrayList<Float> scores;
   
   int padding = 10;
   int threshold = 100;
   float score = 0;
   float lastScore=0;
   float maxScore=0;
   float factor = (float) (3.0/5.0);
   
   ScoreBoard(PGraphics g){
     this.g = g;
     topView = createGraphics(height/8-padding*2,height/8-padding*2,P2D);
     scoreboard = createGraphics(width/10,topView.height,P2D);
     barChart = createGraphics(g.width-topView.width-scoreboard.width-4*padding,topView.height-30,P2D);
     list = new LinkedList<Pair>();
     scores = new ArrayList<Float>();
     //Scrollbar
     bar = new HScrollBar(0,0,barChart.width,topView.height-barChart.height-padding);
     //PGraphics containing the scrollbar
     scrollbar = createGraphics(barChart.width,topView.height-barChart.height-padding,P2D);
   }
  
   void display(){
       pushMatrix();
       translate(0, ((3*height)/8), (int)(Game.depth - ((height/2.0) / tan((float) (PI*30.0 / 180.0)))));
       g.beginDraw();
       g.fill(160);
       g.noStroke();
       g.rect(0, 0, width, (height/8));
       g.endDraw();
       
       image(g,-width/2, 0);
      
       //bar.update();
       drawTopView();
       image(topView,padding-width/2,padding);
       drawScoreBoard();
       image(scoreboard,2*padding-width/2+topView.width,padding);
       drawBarChart();
       image(barChart,3*padding-width/2+topView.width+scoreboard.width,padding);
       drawScrollBar();
       image(scrollbar,-2*padding-width/4,barChart.height+2*padding);

       popMatrix();
    } 
    
    void addScore(float x){
          score+=x;
          lastScore=x;
          if(score<0)score=0;
          
    }
    


void drawTopView(){
   
       topView.beginDraw();
       //topView.background(6,101,130);
       topView.fill(6,101,130);
       topView.noStroke();
       topView.rect(0, 0, topView.width, topView.height);
       
       

      
      //Dessin de la trainée
      topView.pushMatrix();
      topView.noStroke();
      topView.fill(76,171,180);
      
      if(list.size()==threshold){
        list.remove();
        }
        list.add(new Pair(new Float(map(Game.mover.location.x,-(Game.boxEdge/2+padding),(Game.boxEdge/2+padding),0,topView.width)),
                          new Float(map(Game.mover.location.z,-(Game.boxEdge/2+padding),(Game.boxEdge/2+padding),0,topView.height))));
        
        for(Pair p:list){
          topView.ellipse(p.x.floatValue(),p.y.floatValue(),topView.width/(Game.boxEdge/(2.f*Game.radius))*factor,topView.height/(Game.boxEdge/(2.f*Game.radius))*factor);
        }
        topView.popMatrix();
        
       //Dessin de la balle
       topView.fill(178,24,56);
       topView.ellipse(map(Game.mover.location.x,-(Game.boxEdge/2+padding),(Game.boxEdge/2+padding),0,topView.width),
                       map(Game.mover.location.z,-(Game.boxEdge/2+padding),(Game.boxEdge/2+padding),0,topView.height),
                       topView.width/(Game.boxEdge/(2.f*Game.radius)),topView.height/(Game.boxEdge/(2.f*Game.radius)));
      
       //Dessin des cylindres
       topView.fill(230,226,175);
       for(Cylinder c : Game.adder.cylinders){
         topView.ellipse(map(c.x,-(Game.boxEdge/2+padding),(Game.boxEdge/2+padding),0,topView.width),
                       map(c.y,-(Game.boxEdge/2+padding),(Game.boxEdge/2+padding),0,topView.height),
                       topView.width/(Game.boxEdge/(2.f*Game.cylinderBaseSizeInGame)),topView.height/(Game.boxEdge/(2.f*Game.cylinderBaseSizeInGame)));
       }

       topView.endDraw(); 
  
}
void drawScoreBoard(){
   
       scoreboard.beginDraw();
       //scoreboard.background(244);
       scoreboard.fill(244);
       scoreboard.noStroke();
       scoreboard.rect(0, 0, scoreboard.width, scoreboard.height);
       scoreboard.fill(0);
       scoreboard.text("Total score : \n"+Double.toString(Math.floor(score * 1000) / 1000),scoreboard.width/5,scoreboard.height/6);
       scoreboard.text("Velocity : \n"+Double.toString(Math.floor(Game.mover.velocity.mag() * 1000) / 1000),scoreboard.width/5,(int)(scoreboard.height/2.25));
       scoreboard.text("Last score : \n"+Double.toString(Math.floor(lastScore * 1000) / 1000),scoreboard.width/5,(int)(scoreboard.height/1.33));
       scoreboard.endDraw();
}

void drawBarChart(){
    
       barChart.beginDraw();
       //barChart.background(250,246,195);
       barChart.fill(250,246,195);
       barChart.noStroke();
       barChart.rect(0, 0, barChart.width, barChart.height);
       
       barChart.stroke(204, 102, 0);
       barChart.fill(255, 255, 255);
       
       scores.add(score);
       
       for(Float f:scores)
       {
          if(f.floatValue()>maxScore)maxScore=f.floatValue();
       }
       
       barChart.beginShape();
       barChart.vertex(0,barChart.height);
       for(int i=0;i<scores.size(); ++i){
         barChart.vertex(i/5 * bar.getPos(), barChart.height-scores.get(i).floatValue());
       }
       barChart.vertex((scores.size()/5) * bar.getPos(),barChart.height);
       barChart.endShape();
       
       barChart.endDraw();
}
void drawScrollBar(){
       scrollbar.beginDraw();
       bar.update();
       bar.display(scrollbar);
       println(bar.getPos());
       scrollbar.endDraw();
//       bar.update();
//       bar.display(scrollbar);
}

public class Pair { 
  public final Float x; 
  public final Float y; 
  public Pair(Float x, Float y) { 
    this.x = x; 
    this.y = y; 
  } 
} 
}

