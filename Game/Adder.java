package game;

import java.util.ArrayList;

import processing.core.*;

//The group of all the cylinders on the plane

@SuppressWarnings("serial")
class Adder extends PApplet{
  //Place where all the cylinders are stored
  ArrayList<Cylinder> cylinders;
  //Basic constructor
  Adder(){
    this.cylinders = new ArrayList<Cylinder>();
  }
  
  //Method to add a new cylinder
  void add(float x, float y){
    //Check that the ball is not touched or covered by the new cylinder
    if (new PVector(Game.mover.location.x, Game.mover.location.z).dist(new PVector(x, y)) <= (Game.radius + Game.cylinderBaseSizeInGame)) return;
    //Check that the new cylinder is not touching or covering the rest of the cylinders
    for(Cylinder temp: cylinders) {
      if (new PVector(temp.x, temp.y).dist(new PVector(x, y)) <= (2 * Game.cylinderBaseSizeInGame)) return;
    }
    //If all good, put the cylinder in
    Cylinder c = new Cylinder(x,y);
    c.setup();
    cylinders.add(c);
  }
  
  //Method to draw all the cylinders one by one without interfering the systen of coordinated before and after
  void display(){
    for(Cylinder c:cylinders){
      pushMatrix();
      c.display();
      popMatrix();
    }
  }
}


