package game;


import processing.core.PApplet;
import processing.core.PImage;

@SuppressWarnings("serial")
public class Border extends PApplet {
public static class Sobel{
	private PImage img;
	private PImage result;
	private PImage threshold;
	private PImage gaussianImg;
	private float[] buffer;
	private int thresholdHUED,thresholdHUEU,thresholdBrightnessD,thresholdBrightnessU,thresholdSatu;
	
	//Matrix for Gaussian Blur
	private int gaussian[][]  = {
			{9,12,9},
			{12,15,12},
			{9,12,9}
	};
	
	//Matrix for horizontal Kernel for Sobel Algorothm
	private int kernelY[][]  = {
			{-1,-2,-1},
			{0,0,0},
			{1,2,1}
	};
	
	//Matrix for vertical Kernel for Soble Algorithm
	private int kernelX[][]  = {
			{-1,0,1},
			{-2,0,2},
			{-1,0,1}
	};
	
	public Sobel(PImage file,PApplet p){
		
	    //Thresholds for Sobel Algorithm
		thresholdHUED=100;
		thresholdHUEU=135;
		thresholdBrightnessD=20;
		thresholdBrightnessU=230;
		thresholdSatu=100;

		//Input image
		img = file;
		
		//Output images
		result = p.createImage(img.width, img.height, ALPHA);
		threshold = p.createImage(img.width,img.height,ALPHA);
		gaussianImg = p.createImage(img.width,img.height,ALPHA);
		
		buffer = new float[img.width * img.height];
		
	}
}
	public static PImage sobel(PImage file,PApplet p) {
		Sobel s = new Sobel(file,p);
		
		//First iteration over the image applying the green HUE, brightnaess and saturation threshold
		for(int y = 2; y < (s.img.height - 2); y++){
			int temp = y * s.img.width;
			for(int x = 2; x < (s.img.width - 2); x++){
				int pixel = s.img.pixels[temp + x];
				s.gaussianImg.pixels[y * s.img.width + x] = ((p.hue(pixel) > s.thresholdHUED) && (p.hue(pixel) < s.thresholdHUEU) &&
				        (p.brightness(pixel) > s.thresholdBrightnessD) && (p.brightness(pixel) < s.thresholdBrightnessU) &&
				        (p.saturation(pixel) > s.thresholdSatu))? p.color(255): p.color(0);	
			}
		}
		//Second iteration over the image applying the gaussian blur
		for(int y = 2; y < (s.img.height - 2); y++){
			for(int x = 2; x < (s.img.width - 2); x++){
			    
				float convertResult = 0;

				float weight = 1.f;

				for (int i = 0; i < 3; i++){
					for(int j = 0; j < 3; j++){
						float pixel = p.brightness(s.gaussianImg.pixels[(y + j - 1) * s.img.width + (x + i - 1)]);
						convertResult += (pixel * s.gaussian[i][j]); 
						}	}					
				
				convertResult/=weight;
				
				s.threshold.pixels[y * s.img.width + x] = (int) convertResult;
			}
		}
		
		//Sobel algorithm application
		//Only change is in the boundary. We change from [0,length] to [from,to].
		//Therefore each thread is occupied with a slice of image.
		
		for(int y = 2; y < (s.img.height - 2); y++){
			for(int x = 2; x < (s.img.width - 2); x++){
				
				float convertResultX = 0;
				float convertResultY = 0;

				float weight = 1.f;
				
				for (int i = 0; i < 3; i++){
					for(int j  = 0; j < 3; j++){
							float pixel = p.brightness(s.threshold.pixels[(y + j - 1) * s.img.width + (x + i - 1)]);
							convertResultX += pixel*s.kernelX[i][j]; 
							convertResultY += pixel*s.kernelY[i][j]; 
						}						
				}
				convertResultX/=weight;
				convertResultY/=weight;
				
				float temp = Math.round(sqrt(convertResultX * convertResultX + convertResultY * convertResultY));
				s.buffer[y * s.img.width + x] = temp;
			}
			
		}
		
		//Calculating the maximum value
		float maxScore = 0;
		for(float i: s.buffer){
			if(i > maxScore)
				maxScore = i;
		}
		
		//Draw the image according to buffer
		for(int y = 0; y < s.img.height; y++){
			for(int x = 0; x < s.img.width; x++){
				if(s.buffer[y * s.img.width + x]>(int)(maxScore * 0.3)){
					s.result.pixels[y * s.img.width + x] = p.color(255);
				}
				else{
					s.result.pixels[y * s.img.width + x] = p.color(0);
				}
			}
		}
		
		return s.result;
	}
}
