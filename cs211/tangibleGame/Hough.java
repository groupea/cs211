package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import processing.video.Capture;

//import imageprocessing.TwoDThreeD;


@SuppressWarnings("serial")
public class Hough extends PApplet {
	int phiDim;
	int rDim;
	int[] accumulator;
	Capture cam;
	 public void setup() {
	        String[] cameras = Capture.list();
	        cam = new Capture(this, cameras[131]);
	        cam.start();
	    size((int) (800*1.5), 600);
	}
	
	public ArrayList<PVector> hough(PImage edgeImg, int nLines) {
		float discretizationStepsPhi = 0.03f;
		float discretizationStepsR = 2.5f;
		
		// Dimensions of the accumulator
		phiDim = Math.round((float) Math.PI / discretizationStepsPhi);
		rDim = Math.round(((edgeImg.width + edgeImg.height) * 2 + 1) / discretizationStepsR);
		
		// Our accumulator (with a 1 pix margin around)
		accumulator = new int[(phiDim + 2) * (rDim + 2)];
		
		// Fill the accumulator: on edge points (ie, white pixels of the edge
		// image), store all possible (r, phi) pairs describing lines going
		// through the point.
		
		for (int y = 0; y < edgeImg.height; y++) {
			for (int x = 0; x < edgeImg.width; x++) {
				// Are we on an edge?
				if (brightness(edgeImg.pixels[y * edgeImg.width + x]) != 0) {
				// ...determine here all the lines (r, phi) passing through
				// pixel (x,y), convert (r,phi) to coordinates in the
				// accumulator, and increment accordingly the accumulator.
					for(int i = 0; i < (phiDim); ++i){
						double r = (x * Math.cos(i * discretizationStepsPhi)) + (y * Math.sin(i * discretizationStepsPhi));
						int rIdx = (int)Math.round(r / discretizationStepsR);
						rIdx = (rIdx + (rDim - 1)/2) + 1;
						accumulator[((i + 1) * (rDim + 2)) + rIdx]++;
					}
				}
			}
		}
		
		//Displaying the accumulator
		PImage houghImg = createImage(rDim + 2, phiDim + 2, ALPHA);
		
		int maxAcc = 0;   //Maximum Value in the accumulator
		
		for (int i = 0; i < accumulator.length; i++) {
			houghImg.pixels[i] =  color(min(255, accumulator[i]));
			maxAcc = Math.max(maxAcc, accumulator[i]);   // Update the maximum value;
		}
		//PImage bg = loadImage();
		houghImg.updatePixels();  //draw the result in the image
		houghImg.resize(1000, 1000);  //make image of ok size
		
		 //Showing the image: edgeImg is the initial b/w image with boarders and houghImg is the accumulator image
		
		//Choosing the best candidates
		ArrayList<Integer> bestCandidates = new ArrayList<Integer>();
		
		//First method of taking only those that have accumulator value bigger then some percentage of the best value
//		for (int idx = 0; idx < accumulator.length; idx++) {
//		    if (accumulator[idx] >= (maxAcc * 0.3)){
//		        bestCandidates.add(idx);
//		    }
//		}
		
		//Second method of local maxima
		// size of the region we search for a local maximum
		int neighbourhood = 10;
		// only search around lines with more that this amount of votes // (to be adapted to your image)
		int minVotes = (int) (maxAcc * 0.1);
		for (int accR = 0; accR < rDim; accR++) {
		    for (int accPhi = 0; accPhi < phiDim; accPhi++) {
		            // compute current index in the accumulator
		        int idx = (accPhi + 1) * (rDim + 2) + accR + 1;
		        if (accumulator[idx] > minVotes) {
		            boolean bestCandidate=true;
		            // iterate over the neighbourhood
		            for(int dPhi=-neighbourhood/2; dPhi < neighbourhood/2+1; dPhi++) { // check we are not outside the image
		                if( accPhi + dPhi < 0 || accPhi + dPhi >= phiDim) continue;
		                for(int dR=-neighbourhood/2; dR < neighbourhood/2 + 1; dR++) {
		                 // check we are not outside the image
		                    if(accR + dR < 0 || accR + dR >= rDim) continue;
		                    int neighbourIdx = (accPhi + dPhi + 1) * (rDim + 2) + accR + dR + 1;
		                    if(accumulator[idx] < accumulator[neighbourIdx]) { // the current idx is not a local maximum! bestCandidate=false;
		                        break;
		                    }
		                }
		                if(!bestCandidate) break;
		            }
		            if(bestCandidate) {
		            // the current idx *is* a local maximum
		                bestCandidates.add(idx);
		            }
		        }
		    }
		}
		
		//Sort the best candidates using the comparator given below
		Collections.sort(bestCandidates, new HoughComparator(accumulator));
		
		ArrayList<PVector> result = new ArrayList<>();
		//get only nLines best lines
		for (int i = 0; i < nLines; i++) {
		    int idx = bestCandidates.get(i);
		    //if (accumulator[idx] > 200) {
		        // first, compute back the (r, phi) polar coordinates:
		        int accPhi = (int) (idx / (rDim + 2)) - 1;
		        int accR = idx - (accPhi + 1) * (rDim + 2) - 1;
		        float r = (accR - (rDim - 1) * 0.5f) * discretizationStepsR;
		        float phi = accPhi * discretizationStepsPhi;
		        result.add(new PVector(r, phi));
		        // Cartesian equation of a line: y = ax + b
		        // in polar, y = (-cos(phi)/sin(phi))x + (r/sin(phi))
		        // => y = 0 : x = r / cos(phi)
		        // => x = 0 : y = r / sin(phi)
		        // compute the intersection of this line with the 4 borders of // the image
		        int x0 = 0;
		        int y0 = (int) (r / sin(phi));
		        int x1 = (int) (r / cos(phi));
		        int y1 = 0;
		        int x2 = edgeImg.width;
		        int y2 = (int) (-cos(phi) / sin(phi) * x2 + r / sin(phi));
		        int y3 = edgeImg.width;
		        int x3 = (int) (-(y3 - r / sin(phi)) * (sin(phi) / cos(phi)));
		        
		        // Finally, plot the lines
		        stroke(204,102,0);
		        if (y0 > 0) {
		            if (x1 > 0)
		                line(x0, y0, x1, y1);
		            else if (y2 > 0)
		                line(x0, y0, x2, y2);
		            else
		                line(x0, y0, x3, y3);
		        }
		        else {
		            if (x1 > 0) {
		                if (y2 > 0)
		                    line(x1, y1, x2, y2);
		                else
		                    line(x1, y1, x3, y3);
		                 }
		            else
		                line(x2, y2, x3, y3);
		        }
		    //} 
		}
		return result;
		
	}

	public PVector compute() {
	    //Make image to give as input and output image to Hough
		PImage houghImg = createImage(rDim + 2, phiDim + 2, ALPHA);
        cam.read();
	    PImage image = Border.sobel(cam,this);

	    //Building QuadGraph using the output of Hough algorithm
	    QuadGraph quadGr = new QuadGraph();
	    ArrayList<PVector> lines = hough(image, 4);
	    quadGr.build(lines, image.width, image.height);
	    List<int[]> quads = quadGr.findCycles();
	    List<int[]> goodQuads = new ArrayList<>(quads);
	    
	    //Filtering the quads produced
	    for(int[] quad:quads){
	    	PVector l1 = lines.get(quad[0]);
	        PVector l2 = lines.get(quad[1]);
	        PVector l3 = lines.get(quad[2]);
	        PVector l4 = lines.get(quad[3]);
	        
	        PVector c12 = intersection(l1, l2);
	        PVector c23 = intersection(l2, l3);
	        PVector c34 = intersection(l3, l4);
	        PVector c41 = intersection(l4, l1);
	        
	        if(!QuadGraph.isConvex(c12,c23,c34,c41)){
	        	goodQuads.remove(quad);
	        }
	        if(!QuadGraph.validArea(c12,c23,c34,c41, 290000, 100000)){
	        	goodQuads.remove(quad);
	        }
	        if(!QuadGraph.nonFlatQuad(c12,c23,c34,c41)){
	        	goodQuads.remove(quad);
	        }
	    }
	    
	    //Check for number of good Quads produced
	    System.out.println(goodQuads.size());
	    PVector angles = new PVector(0,0,0);
	    //Draw all the good quads
	    try
	    {
	        //
	        //Getting intersections for the transformation
	        List<PVector> quadToUse = new ArrayList<>();
	        //
	    	int[] quad = goodQuads.get(0);
	        PVector l1 = lines.get(quad[0]);
	        PVector l2 = lines.get(quad[1]);
	        PVector l3 = lines.get(quad[2]);
	        PVector l4 = lines.get(quad[3]);
	        // (intersection() is a simplified version of the
	        // intersections() method you wrote last week, that simply
	        // return the coordinates of the intersection between 2 lines)
	        PVector c12 = intersection(l1, l2);
	        PVector c23 = intersection(l2, l3);
	        PVector c34 = intersection(l3, l4);
	        PVector c41 = intersection(l4, l1);
	        //
	        quadToUse.add(c12);
	        quadToUse.add(c23);
	        quadToUse.add(c34);
	        quadToUse.add(c41);
	        System.out.println("Recalculation of the intesections and calculating the rotations:");
	        TwoDThreeD convertion = new TwoDThreeD(image.width, image.height);
	         angles = convertion.get3DRotations(game.CWComparator.sortCorners(quadToUse));
	        System.out.println(Math.toDegrees(angles.x) + ", " + Math.toDegrees(angles.y) + ", " + Math.toDegrees(angles.z) );
	        //
	        // Choose a random, semi-transparent colour
	        Random random = new Random(); fill(color(min(255, random.nextInt(300)),
	                min(255, random.nextInt(300)),
	                min(255, random.nextInt(300)), 50));
	        quad(c12.x,c12.y,c23.x,c23.y,c34.x,c34.y,c41.x,c41.y);
	    }catch(Exception e){System.out.println("Pas de bons quads !");}
	    
	    //Display the images produced
	    
		
		//Intersections and rotation angles
        //System.out.println("Recalculation of the intesections and calculating the rotations:");
        //TwoDThreeD convertion = new TwoDThreeD(image.width, image.height);
        //PVector angles = convertion.get3DRotations(imageprocessing.CWComparator.sortCorners(getIntersections(lines)));
        
		
		return angles;
	}
	
	//Complete method to get Intersections between the lines
	public ArrayList<PVector> getIntersections(List<PVector> lines) {
	    ArrayList<PVector> intersections = new ArrayList<PVector>();
	    for (int i = 0; i < lines.size() - 1; i++) {
	        PVector line1 = lines.get(i);
	            for (int j = i + 1; j < lines.size(); j++) {
	                PVector line2 = lines.get(j);
	                // compute the intersection and add it to 'intersections'
	                float d = (float) (Math.cos(line2.y) * Math.sin(line1.y) - Math.cos(line1.y) * Math.sin(line2.y));
	                float x = (float) (line2.x * Math.sin(line1.y) - line1.x * Math.sin(line2.y)) / d;
	                float y = (float) (-(line2.x * Math.cos(line1.y)) + line1.x * Math.cos(line2.y)) / d;
	                intersections.add(new PVector(x, y));
	                // draw the intersection
	                fill(255, 128, 0);
	                ellipse(x, y, 10, 10);
	            }
	    }
	    return intersections;
	}
	
	//Similar method as getIntersections, but only for two lines
	public PVector intersection(PVector line1, PVector line2) {
        float d = (float) (Math.cos(line2.y) * Math.sin(line1.y) - Math
                .cos(line1.y) * Math.sin(line2.y));
        float x = (float) (line2.x * Math.sin(line1.y) - line1.x
                * Math.sin(line2.y))
                / d;
        float y = (float) (-(line2.x * Math.cos(line1.y)) + line1.x
                * Math.cos(line2.y))
                / d;
        fill(255, 128, 0);
        ellipse(x, y, 10, 10);
        return (new PVector(x, y));
    }
}

//Comparator for values on the accumulator => lines
class HoughComparator implements Comparator<Integer> {
    int[] accumulator;
    
    public HoughComparator(int[] accumulator) {
        this.accumulator = accumulator;
    }
    
    @Override
    public int compare(Integer l1, Integer l2) {
        if (accumulator[l1] > accumulator[l2] || (accumulator[l1] == accumulator[l2] && l1 < l2)) return -1;
        return 1;
    }
}
