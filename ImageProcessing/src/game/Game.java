package game;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.*; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException;

import imageprocessing.*;

@SuppressWarnings("serial")
public class Game extends PApplet {

//The group of all the cylinders on the plane

class Adder{
  //Place where all the cylinders are stored
  ArrayList<Cylinder> cylinders;
  //Basic constructor
  Adder(){
    this.cylinders = new ArrayList<Cylinder>();
  }
  
  //Method to add a new cylinder
  public void add(float x, float y){
    //Check that the ball is not touched or covered by the new cylinder
    if (new PVector(mover.location.x, mover.location.z).dist(new PVector(x, y)) <= (radius + cylinderBaseSizeInGame)) return;
    //Check that the new cylinder is not touching or covering the rest of the cylinders
    for(Cylinder temp: cylinders) {
      if (new PVector(temp.x, temp.y).dist(new PVector(x, y)) <= (2 * cylinderBaseSizeInGame)) return;
    }
    //If all good, put the cylinder in
    Cylinder c = new Cylinder(x,y);
    c.setup();
    cylinders.add(c);
  }
  
  //Method to draw all the cylinders one by one without interfering the systen of coordinated before and after
  public void display(){
    for(Cylinder c:cylinders){
      pushMatrix();
      c.display();
      popMatrix();
    }
  }
}


//Cylinder to put on the plane

class Cylinder{
  //Cylinder parameters
  float cylinderBaseSize = cylinderBaseSizeInGame;
  float cylinderHeight = cylinderHeightInGame;
  int cylinderResolution = cylinderResolutionInGame;
  PShape tree;
  PShape top = new PShape();
  PShape cap = new PShape();
  //Cylinder position
  float x,y;



  
  //Basic constructer
  Cylinder(float x, float y){
    this.x=x;
    this.y=y;
  }
  
  //Method to put cylinder in place
  public void setup(){
    tree = loadShape("/Users/JayD/Documents/Универ/EPFL/IntroVisComp/MainThing/ImageProcessing/src/game/arbre.obj");
  }
  
  //Method to draw the full cylinder wothout interfering the system of coordinates before and after
    public void display(){
    pushMatrix();
    translate(x,y,20);
    scale(20);
    rotateX(PI/2);
    shape(tree);
    popMatrix();
  } 
}
Mover mover;
Adder adder;
Cylinder model;
ScoreBoard scoreBoard;


//General Initial Settings of the Game

float depth = 2000;
int radius = 50;    //Radius of the ball
float rX = 0;      //Rotationangles of the plane
float rZ = 0;
float rY = 0;
float speedFactor = 1;      //Spped control
float boxEdge = 1000;        //Length of the Edge of the plane 
boolean stop = false;      //Flag for buidling mode

//Cylinder Settings for the Game

float cylinderBaseSizeInGame = 50;
float cylinderHeightInGame = 100;
int cylinderResolutionInGame = 40;


public void setup() {
  size(1000, 1000, P3D);
  mover = new Mover();
  model = new Cylinder(0,0);
  adder = new Adder();
  scoreBoard = new ScoreBoard(createGraphics(width, (height/8), P2D));

}

public void draw() {
  camera(width/2, height/2, depth, width/2, height/2, 0, 0, 1, 0);
  background(200);
  translate(width/2, height/2, 0);
  lights();

  
 
  
  //Normal routine
  
  if (!stop){
    scoreBoard.display();
    
    rotateZ(rZ);
    rotateX(rX);
    rotateY(rY);
    box(boxEdge, 40, boxEdge);
   
  //The part that draws Cylinders
   
    pushMatrix();
    rotateX(PI/2);
    translate(0, 0 , 20);
    adder.display();
    popMatrix();
    
  //End of the part that draws Cylinders
  
  //Part that draws the ball
  
    translate(0, -(radius + 20), 0);
    mover.checkEdges();
    mover.checkCylinderCollision();
    mover.update();
    mover.display();
    
  //End of the part that draws the ball
  
  } else {
    
  //Building Mode Routine
    
    //Bring the plane up the camepa to avoid perspective
    translate(0, 0, (depth - ((height/2.0f) / tan(PI*30.0f / 180.0f))) - 20);
    //Draw plane and cylinders
    box(boxEdge, boxEdge, 40);
    adder.display();
    pushMatrix();
    //Using the fact that plane is up close, it is the one to one relation except for signs
    //Hence we use mapping to correct the signs and display the cylinder where it would be placed
    translate(map(mouseX, 0, width, -(width/2), (width/2)), map(mouseY, 0, height, -(height/2), (height/2)), 0);
    model.setup();
    model.display();
    popMatrix();
    //Going back to the plane system of coordinates and draw the ball as usual
    pushMatrix();
    translate(mover.location.x, mover.location.z, (radius - 20));
    sphere(radius);
    popMatrix();
  }
  
  
}

public void mouseDragged() {
  //Tools outside of the Building Mode
  if (!stop&&mouseY<7*height/8){
    //Rotating the plane around X axis ensuring the boundaries of abs(andle) < 30 degrees (considering speedFactor)
    float rXint = speedFactor*(map(mouseY, (0), (height), (PI/3), (-PI/3)));
    if (rXint >= 0) {
      rX = min(rXint, (PI/3));
    } else {
      rX = max(rXint, (-PI/3));
    }
    //Rotating the plane around Z axis ensuring the boundaries of abs(andle) < 30 degrees (considering speedFactor)
    float rZint = speedFactor*(map(mouseX, (0), (width), (-PI/3), (PI/3)));
    if (rZint >= 0) {
      rZ = min(rZint, (PI/3));
    } else {
      rZ = max(rZint, (-PI/3));
    }
  }
}

public void keyPressed() {
  if (key == CODED) {
    //Rotating the plane aroundY axis without any boundaries (considering sppedFactor)
    if (keyCode == LEFT) {
      rY = rY - ((PI/12) * abs(speedFactor));
    } else if (keyCode == RIGHT) {
      rY = rY + ((PI/12) * abs(speedFactor));
    }
    //Setting the Building Moode
    if (keyCode == SHIFT) {
      stop = true;
    }
  }
}

public void mousePressed(){ 
 
  //Once in Building Mode: the tool to put in place the cylinders
  //Since mouse axises are only positive we need to remap them to
  //allow negative values that we may directly use for cylinder
  //setting as the relation is one to one since we brought the
  //plane up to the camera when we entered the building mode.
 
  if(stop){
      // We make sure that we can put cylinders on the plane iff they will be fully on it
      float cylX = map(mouseX, 0, width, -(width/2), (width/2));    //x coordinate of the future cylinder
      float cylY = map(mouseY, 0, height, -(height/2), (height/2));  //y coordinate of the future cylinder
      float allowedDist = (boxEdge/2 - (cylinderBaseSizeInGame)); //distance from the center ensuring the condition
      println("cylX " + cylX + " , cylY " + cylY + " allowedDist " + allowedDist);
      if((cylX < allowedDist) && (cylX > -(allowedDist)) &&
        (cylY < allowedDist) && (cylY > -(allowedDist))){
          adder.add(cylX, cylY);
       }
  } 
}

public void keyReleased(){
  
  //Once SHIFT is released we exit the Building Mode
  
  if (keyCode == SHIFT){
   stop = false; 
  }
}

public void mouseWheel(MouseEvent event) {
  
  //Using the mouse wheel we may adjust the speed of the rotations by changing the speedFactor
  
  float e = event.getCount();
  speedFactor = min(speedFactor * map(e, 100, -100, 0.00001f, 2), 50);
}

class HScrollBar {
  float barWidth;  //Bar's width in pixels
  float barHeight; //Bar's height in pixels
  float xPosition;  //Bar's x position in pixels
  float yPosition;  //Bar's y position in pixels
  
  float sliderPosition, newSliderPosition;    //Position of slider
  float sliderPositionMin, sliderPositionMax; //Max and min values of slider
  
  boolean mouseOver;  //Is the mouse over the slider?
  boolean locked;     //Is the mouse clicking and dragging the slider now?

  /**
   * @brief Creates a new horizontal scrollbar
   * 
   * @param x The x position of the top left corner of the bar in pixels
   * @param y The y position of the top left corner of the bar in pixels
   * @param w The width of the bar in pixels
   * @param h The height of the bar in pixels
   */
  HScrollBar (float x, float y, float w, float h) {
    barWidth = w;
    barHeight = h;
    xPosition = x;
    yPosition = y;
    
    sliderPosition = xPosition + barWidth/2 - barHeight/2;
    newSliderPosition = sliderPosition;
    
    sliderPositionMin = xPosition;
    sliderPositionMax = xPosition + barWidth - barHeight;
  }

  /**
   * @brief Updates the state of the scrollbar according to the mouse movement
   */
  public void update() {
    if (isMouseOver()) {
      mouseOver = true;
    }
    else {
      mouseOver = false;
    }
    if (mousePressed && mouseOver) {
      locked = true;
    }
    if (!mousePressed) {
      locked = false;
    }
    if (locked) {
      newSliderPosition = constrain(mouseX - barHeight/2 + (7*scoreBoard.padding-width/2+scoreBoard.topView.width+scoreBoard.scoreboard.width), sliderPositionMin, sliderPositionMax);
    }
    if (abs(newSliderPosition - sliderPosition) > 1) {
      sliderPosition = sliderPosition + (newSliderPosition - sliderPosition);
    }
  }

  /**
   * @brief Clamps the value into the interval
   * 
   * @param val The value to be clamped
   * @param minVal Smallest value possible
   * @param maxVal Largest value possible
   * 
   * @return val clamped into the interval [minVal, maxVal]
   */
  public float constrain(float val, float minVal, float maxVal) {
    return min(max(val, minVal), maxVal);
  }

  /**
   * @brief Gets whether the mouse is hovering the scrollbar
   *
   * @return Whether the mouse is hovering the scrollbar
   */
  public boolean isMouseOver() {
    if (mouseX > xPosition-(7*scoreBoard.padding-width/2+scoreBoard.topView.width+scoreBoard.scoreboard.width) && mouseX < xPosition+barWidth-(7*scoreBoard.padding-width/2+scoreBoard.topView.width+scoreBoard.scoreboard.width) &&
      mouseY > yPosition+height-3*scoreBoard.padding && mouseY < yPosition+barHeight+height-3*scoreBoard.padding) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * @brief Draws the scrollbar in its current state
   */ 
  public void display(PGraphics g) {
    g.pushMatrix();
    g.noStroke();
    g.fill(204);
    g.rect(xPosition, yPosition, barWidth, barHeight);
    if (mouseOver || locked) {
      g.fill(0, 0, 0);
    }
    else {
      g.fill(102, 102, 102);
    }
    g.rect(sliderPosition, yPosition, barHeight, barHeight);
    g.popMatrix();
  }

  /**
   * @brief Gets the slider position
   * 
   * @return The slider position in the interval [0,1] corresponding to [leftmost position, rightmost position]
   */
  public float getPos() {
    return (sliderPosition - xPosition)/(barWidth - barHeight);
  }
}

//The ball and all the forces acting on it:
//- Gravity
//- Friction
//Also calculates the collisions with cylinders and the boarders

class Mover {
  PVector location;
  PVector velocity;
  PVector gravityForce; 
  
  //Constants for calculation of the velocity changes
  float gravityConstant = 0.5f;
  float elasticity = 0.60f;
  float normalForce = 1;
  float mu = 0.01f;
  float frictionMagnitude = normalForce * mu;

  //Default positions and directions
  Mover() {
    location = new PVector(0, 0, 0);
    velocity = new PVector(0, 0, 0);
    gravityForce = new PVector(0, 0, 0);
  }
  
  //Re-evaluating the force vectors and directions and applying the
  //effect on the velocity and calculating new position of the ball
  
  public void update() {
    //New gravity depends on the rotation of the plane
    gravityForce.x = sin(rZ) * gravityConstant;
    gravityForce.z = -sin(rX) * gravityConstant;
    //Friction always has the same magnitude and is in opposite direction of the velocity
    PVector friction = velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    //At this stage gravityForce represents the overall force acting on the ball
    //so we add friction to it and get the total force which may directly used as
    //accelaration as we don't consider mass of the ball. Moreover we even asssume
    //that delta time is equal to 1 so in our case force is change in velocity
    gravityForce.add(friction);
    velocity.add(gravityForce);
    //New position is calculated by applying the velocity, since we assume delta time
    //equal zero => we just add the velocity vector to the position vector
    location.add(velocity);
  }
  
  //Method to display the ball without intervining the system of coordinates before and after
  
  public void display() {
    pushMatrix();
    translate(location.x, location.y, location.z);
    sphere(radius);
    popMatrix();
  }
  
  //Method for cheking if the ball went over the edge on the plane and put it back in
  
  public void checkEdges() {
    //Right Edge
    if ((location.x + radius) > (boxEdge/2)) {
      velocity.x = -(velocity.x * elasticity);
      location.x = (boxEdge/2) - radius;
      scoreBoard.addScore(-velocity.mag());
    } else if ((location.x - radius) < -(boxEdge/2)) {
      //Left Edge
      velocity.x = -(velocity.x * elasticity);
      location.x = -(boxEdge/2) + radius;
      scoreBoard.addScore(-velocity.mag());
    }
    //Far Edge
    if ((location.z + radius) > (boxEdge/2)) {
      velocity.z = -(velocity.z * elasticity);
      location.z = (boxEdge/2) - radius;
      scoreBoard.addScore(-velocity.mag());
    } else if ((location.z - radius) < -(boxEdge/2)) {
      //Close Edge
      velocity.z = -(velocity.z * elasticity);
      location.z = -(boxEdge/2) + radius;
      scoreBoard.addScore(-velocity.mag());
    }
  }
  
  //Check for the collision with the cylinders and put the ball at right place
  
  public void checkCylinderCollision(){
    //Need to check every cylinder
    for (Cylinder c: adder.cylinders ){
      //z coordinate of the ball corresponds to the y coordinate of the culinder
      //Take into cinsideration of the radius of the ball, and the radius of the base of the cylinder
      //Need to check each separately because collision may occure in multiple directions at the same time
      //Collision of the ball with the closer side of the cylinder
      if (((location.z + radius) > (c.y - c.cylinderBaseSize)) &&
          !(location.z > c.y) &&
          (location.x < (c.x + c.cylinderBaseSize)) &&
          (location.x > (c.x - c.cylinderBaseSize))) {
            velocity.z = -(velocity.z * elasticity);
            location.z = (c.y - c.cylinderBaseSize - radius);
            scoreBoard.addScore(velocity.mag());
      }
      //Collision of the ball with the further side of the cylinder
      if (((location.z - radius) < (c.y + c.cylinderBaseSize)) &&
          !(location.z < c.y) &&
          (location.x < (c.x + c.cylinderBaseSize)) &&
          (location.x > (c.x - c.cylinderBaseSize))) {
            velocity.z = -(velocity.z * elasticity);
            location.z = (c.y + c.cylinderBaseSize + radius);
            scoreBoard.addScore(velocity.mag());
      }
      //Collision of the ball with the left side of the cylinder
      if (((location.x + radius) > (c.x - c.cylinderBaseSize)) &&
          !(location.x > c.x) &&
          (location.z < (c.y + c.cylinderBaseSize)) &&
          (location.z > (c.y - c.cylinderBaseSize))) {
            velocity.x = -(velocity.x * elasticity);
            location.x = (c.x - c.cylinderBaseSize - radius);
            scoreBoard.addScore(velocity.mag());
      }
      //Collision of the ball with the right side of the culinder
      if (((location.x - radius) < (c.x + c.cylinderBaseSize)) &&
          !(location.x < c.x) &&
          (location.z < (c.y + c.cylinderBaseSize)) &&
          (location.z > (c.y - c.cylinderBaseSize))) {
            velocity.x = -(velocity.x * elasticity);
            location.x = (c.x + c.cylinderBaseSize + radius);
            scoreBoard.addScore(velocity.mag());
      }
      
      //Check that ball is not partly inside the cylinder once collided
      if(new PVector(location.x, location.z).dist(new PVector(c.x, c.y)) <= (radius + c.cylinderBaseSize)){
        //Manage location
        PVector n = new PVector(location.x, 0, location.z);
        n.sub(c.x, 0, c.y);
        n.normalize();
        PVector n2 = n.get();
        n2.mult(radius + c.cylinderBaseSize);
        n2.add(c.x, 0, c.y);
        location = n2;
        //Manage velocity
        float x = (2 * velocity.dot(n));
        n.mult(x);
        velocity.sub(n);
        velocity.mult(elasticity);
        scoreBoard.addScore(velocity.mag());
      }
    }
  }
}

//TODO :  - vitesse de la sph\u00e8re = 0.2-0.3 quand idle 
//        - mapper la valeur de la barchart sur le maximum => map(x,0,maxScore,0,barChart.height); ou similaire
class ScoreBoard{
   
  //Principal background
   PGraphics g;
   PGraphics topView;
   PGraphics scoreboard;
   PGraphics barChart;
   PGraphics scrollbar;
   
   HScrollBar bar;
   
   LinkedList<Pair> list;
   ArrayList<Float> scores;
   
   int padding = 10;
   int threshold = 100;
   float score = 0;
   float lastScore=0;
   float maxScore=0;
   float factor = 3.0f/5.0f;
   
   ScoreBoard(PGraphics g){
     this.g = g;
     topView = createGraphics(height/8-padding*2,height/8-padding*2,P2D);
     scoreboard = createGraphics(width/10,topView.height,P2D);
     barChart = createGraphics(g.width-topView.width-scoreboard.width-4*padding,topView.height-30,P2D);
     list = new LinkedList<Pair>();
     scores = new ArrayList<Float>();
     //Scrollbar
     bar = new HScrollBar(0,0,barChart.width,topView.height-barChart.height-padding);
     //PGraphics containing the scrollbar
     scrollbar = createGraphics(barChart.width,topView.height-barChart.height-padding,P2D);
   }
  
   public void display(){
       pushMatrix();
       translate(0, ((3*height)/8), (depth - ((height/2.0f) / tan(PI*30.0f / 180.0f))));
       g.beginDraw();
       //g.background(160);
       g.fill(160);
       g.noStroke();
       g.rect(0, 0, width, (height/8));
       g.endDraw();
       
       image(g,-width/2, 0);
      
       //bar.update();
       drawTopView();
       image(topView,padding-width/2,padding);
       drawScoreBoard();
       image(scoreboard,2*padding-width/2+topView.width,padding);
       drawBarChart();
       image(barChart,3*padding-width/2+topView.width+scoreboard.width,padding);
       drawScrollBar();
       image(scrollbar,-2*padding-width/4,barChart.height+2*padding);

       popMatrix();
    } 
    
    public void addScore(float x){
          score+=x;
          lastScore=x;
          if(score<0)score=0;
          
    }
    


public void drawTopView(){
   
       topView.beginDraw();
       //topView.background(6,101,130);
       topView.fill(6,101,130);
       topView.noStroke();
       topView.rect(0, 0, topView.width, topView.height);
       
       

      
      //Dessin de la train\u00e9e
      topView.pushMatrix();
      topView.noStroke();
      topView.fill(76,171,180);
      
      if(list.size()==threshold){
        list.remove();
        }
        list.add(new Pair(new Float(map(mover.location.x,-(boxEdge/2+padding),(boxEdge/2+padding),0,topView.width)),
                          new Float(map(mover.location.z,-(boxEdge/2+padding),(boxEdge/2+padding),0,topView.height))));
        
        for(Pair p:list){
          topView.ellipse(p.x.floatValue(),p.y.floatValue(),topView.width/(boxEdge/(2.0f*radius))*factor,topView.height/(boxEdge/(2.0f*radius))*factor);
        }
        topView.popMatrix();
        
       //Dessin de la balle
       topView.fill(178,24,56);
       topView.ellipse(map(mover.location.x,-(boxEdge/2+padding),(boxEdge/2+padding),0,topView.width),
                       map(mover.location.z,-(boxEdge/2+padding),(boxEdge/2+padding),0,topView.height),
                       topView.width/(boxEdge/(2.0f*radius)),topView.height/(boxEdge/(2.0f*radius)));
      
       //Dessin des cylindres
       topView.fill(230,226,175);
       for(Cylinder c : adder.cylinders){
         topView.ellipse(map(c.x,-(boxEdge/2+padding),(boxEdge/2+padding),0,topView.width),
                       map(c.y,-(boxEdge/2+padding),(boxEdge/2+padding),0,topView.height),
                       topView.width/(boxEdge/(2.0f*cylinderBaseSizeInGame)),topView.height/(boxEdge/(2.0f*cylinderBaseSizeInGame)));
       }

       topView.endDraw(); 
  
}
public void drawScoreBoard(){
   
       scoreboard.beginDraw();
       //scoreboard.background(244);
       scoreboard.fill(244);
       scoreboard.noStroke();
       scoreboard.rect(0, 0, scoreboard.width, scoreboard.height);
       scoreboard.fill(0);
       scoreboard.text("Total score : \n"+Double.toString(Math.floor(score * 1000) / 1000),scoreboard.width/5,scoreboard.height/6);
       scoreboard.text("Velocity : \n"+Double.toString(Math.floor(mover.velocity.mag() * 1000) / 1000),scoreboard.width/5,scoreboard.height/2.25f);
       scoreboard.text("Last score : \n"+Double.toString(Math.floor(lastScore * 1000) / 1000),scoreboard.width/5,scoreboard.height/1.33f);
       scoreboard.endDraw();
}

public void drawBarChart(){
    
       barChart.beginDraw();
       //barChart.background(250,246,195);
       barChart.fill(250,246,195);
       barChart.noStroke();
       barChart.rect(0, 0, barChart.width, barChart.height);
       
       barChart.stroke(204, 102, 0);
       barChart.fill(255, 255, 255);
       
       scores.add(score);
       
       for(Float f:scores)
       {
          if(f.floatValue()>maxScore)maxScore=f.floatValue();
       }
       
       barChart.beginShape();
       barChart.vertex(0,barChart.height);
       for(int i=0;i<scores.size(); ++i){
         barChart.vertex(i/5 * bar.getPos(), barChart.height-scores.get(i).floatValue());
       }
       barChart.vertex((scores.size()/5) * bar.getPos(),barChart.height);
       barChart.endShape();
       
       barChart.endDraw();
}
public void drawScrollBar(){
       scrollbar.beginDraw();
       bar.update();
       bar.display(scrollbar);
       println(bar.getPos());
       scrollbar.endDraw();
//       bar.update();
//       bar.display(scrollbar);
}

public class Pair { 
  public final Float x; 
  public final Float y; 
  public Pair(Float x, Float y) { 
    this.x = x; 
    this.y = y; 
  } 
} 
}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "game" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
