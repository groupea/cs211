package sobel;

import java.util.LinkedList;

import processing.core.PApplet;
import processing.core.PImage;


@SuppressWarnings("serial")
public class Sobel extends PApplet {

	private static PImage img = null;
	private PImage result;
	private HScrollbar scroll1;
	private HScrollbar scroll2;
	private PImage threshold;
	private float[] buffer;
	
	
	
	public void setup(){
		loop();
		String filename = "C:/Users/Colin/workspace/Sobel/src/data/board1.jpg";
		img = loadImage(filename);
		result = createImage(img.width, img.height, ALPHA);
		threshold = createImage(img.width,img.height,ALPHA);
		size(img.width,img.height);
		scroll1= new HScrollbar(this, 0, img.height-50, img.width, 20);
		scroll2= new HScrollbar(this, 0, img.height-20, img.width, 20);
		
		buffer = new float[img.width*img.height];
		
	}
	
	public void draw() {
		
		int kernelY[][]  = {
				{-1,-2,-1},
				{0,0,0},
				{1,2,1}
		};
		int kernelX[][]  = {
				{-1,0,1},
				{-2,0,2},
				{-1,0,1}
		};

		int threshold1 = (int) (scroll1.getPos()*255);
		int threshold2 = (int) (scroll2.getPos()*255);
		
		for(int y=2;y<img.height-2;y++){
			for(int x=2;x<img.width-2;x++){
				int pixel = img.pixels[y*img.width+x];
				//threshold.pixels[y*img.width+x]=(brightness(pixel)>threshold1&&brightness(pixel)<threshold2)?color(255):color(0);	
			}
		}
		//Only change is in the boundary. We change from [0,length] to [from,to].
		//Therefore each thread is occupied with a slice of image.
		for(int y=2;y<img.height-2;y++){
			for(int x=2;x<img.width-2;x++){
				
				
				float convertResultX = 0;
				float convertResultY = 0;

				float weight = 1.f;
				

				for (int i = 0; i<3; i++){
					for(int j  = 0; j<3; j++){
						float pixel = brightness(img.pixels[(y+j-1)*img.width+(x+i-1)]);
						convertResultX += pixel*kernelX[i][j]; 
						convertResultY += pixel*kernelY[i][j]; 
						}						
				}
				convertResultX/=weight;
				convertResultY/=weight;
				
				float temp = Math.round(sqrt(convertResultX*convertResultX+convertResultY*convertResultY));
				buffer[y*img.width+x]=temp;
			}
			
			
		}
		float maxScore = 0;
		for(float i:buffer){
			if(i>maxScore)
				maxScore=i;
		}
		for(int y=2;y<img.height-2;y++){
			for(int x=2;x<img.width-2;x++){
				if(buffer[y*img.width+x]>(int)(maxScore*0.3)){
					result.pixels[y*img.width+x]=color(255);
				}
				else{
					result.pixels[y*img.width+x]=color(0);
				}
			}
		}
		image(result,0,0);
		/*scroll1.update();
		scroll1.display();
		scroll2.update();
		scroll2.display();*/
	}
}
