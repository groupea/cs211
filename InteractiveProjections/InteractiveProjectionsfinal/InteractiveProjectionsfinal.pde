float rX;
float rY;
float scale = 1;
void setup() {

  size(1000,1000,P2D);
}
void draw() {
  
  background(255, 255, 255);
 My3DPoint  eye = new My3DPoint(0, 0, -5000);
   My3DPoint origin = new My3DPoint(width/2-150/2, height/2-150/2, 0);
   My3DBox input3DBox = new My3DBox(origin, 150, 150, 150);


  float[][] transform = rotateXMatrix(rX);
  input3DBox = transformBox(input3DBox, transform);
  
  float[][] transform1 = rotateYMatrix(rY);
  input3DBox = transformBox(input3DBox, transform1);
  
  float[][] transform2 = scaleMatrix(scale, scale, scale); 
  input3DBox = transformBox(input3DBox, transform2);
  
  projectBox(eye, input3DBox).render();
}

class My2DPoint {
  float x;
  float y;
  My2DPoint(float x, float y) {
    this.x = x;
    this.y = y;
  }
}

class My3DPoint {
  float x;
  float y;
  float z;
  My3DPoint(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
   }
}
My2DPoint projectPoint(My3DPoint eye, My3DPoint p) {
  float xp = -eye.z*(p.x-eye.x)/(p.z-eye.z);
  float yp = -eye.z*(p.y-eye.y)/(p.z-eye.z);
  return new My2DPoint(xp, yp);
}
class My2DBox {
  My2DPoint[] s;
  My2DBox(My2DPoint[] s) {
    this.s = s;
  }
  void render(){
    line(s[0].x, s[0].y, s[1].x, s[1].y);
    line(s[0].x, s[0].y, s[3].x, s[3].y);
    line(s[0].x, s[0].y, s[4].x, s[4].y);
    
    line(s[1].x, s[1].y, s[2].x, s[2].y);
    line(s[1].x, s[1].y, s[5].x, s[5].y);
    
    line(s[2].x, s[2].y, s[3].x, s[3].y);
    line(s[2].x, s[2].y, s[6].x, s[6].y);
    
    line(s[3].x, s[3].y, s[7].x, s[7].y);
    
    line(s[4].x, s[4].y, s[5].x, s[5].y);
    line(s[4].x, s[4].y, s[7].x, s[7].y);
    
    line(s[5].x, s[5].y, s[6].x, s[6].y);
    
    line(s[6].x, s[6].y, s[7].x, s[7].y);
  }
}
class My3DBox {
  My3DPoint[] p;
  My3DBox(My3DPoint origin, float dimX, float dimY, float dimZ){
    float x = origin.x;
    float y = origin.y;
    float z = origin.z;
    this.p = new My3DPoint[]{new My3DPoint(x,y+dimY,z+dimZ),
                             new My3DPoint(x,y,z+dimZ),
                             new My3DPoint(x+dimX,y,z+dimZ),
                             new My3DPoint(x+dimX,y+dimY,z+dimZ),
                             new My3DPoint(x,y+dimY,z),
                             origin,
                             new My3DPoint(x+dimX,y,z),
                             new My3DPoint(x+dimX,y+dimY,z)
                             };
  }
  My3DBox(My3DPoint[] p) {
    this.p = p;
  }
}
My2DBox projectBox (My3DPoint eye, My3DBox box) {
  My3DPoint[] p = box.p;
  My2DPoint[] s = new My2DPoint[]{
    projectPoint(eye, p[0]),
    projectPoint(eye, p[1]),
    projectPoint(eye, p[2]),
    projectPoint(eye, p[3]),
    projectPoint(eye, p[4]),
    projectPoint(eye, p[5]),
    projectPoint(eye, p[6]),
    projectPoint(eye, p[7])
    };
  return new My2DBox(s);
}
float[] homogeneous3DPoint (My3DPoint p) {
  float[] result = {p.x, p.y, p.z , 1};
  return result;
}
float[][] rotateXMatrix(float angle) {
  return(new float[][] {{1, 0 , 0 , 0},
                        {0, cos(angle), sin(angle) , 0},
                        {0, -sin(angle) , cos(angle) , 0},
                        {0, 0 , 0 , 1}});
}
float[][] rotateYMatrix(float angle) {
return(new float[][] {{cos(angle), 0 , -sin(angle), 0},
                      {0, 1, 0, 0},
                      {sin(angle), 0, cos(angle) , 0},
                      {0, 0 , 0 , 1}});
}
float[][] rotateZMatrix(float angle) {
return(new float[][] {{cos(angle),sin(angle) , 0 , 0},
                      {-sin(angle), cos(angle), 0 , 0},
                      {0, 0 ,1 , 0},
                      {0, 0 , 0 , 1}});
}
float[][] scaleMatrix(float x, float y, float z) {
 return(new float[][] {{x,0 , 0 , 0},
                      {0, y, 0 , 0},
                      {0, 0 ,z , 0},
                      {0, 0 , 0 , 1}});
}
float[][] translationMatrix(float x, float y, float z) {
  return(new float[][] {{1,0 , 0 , x},
                        {0, 1, 0 , y},
                        {0, 0 ,1 , z},
                        {0, 0 , 0 , 1}});
}
float[] matrixProduct(float[][] a, float[] b) {
  return(new float[] {
    a[0][0]*b[0]+a[0][1]*b[1]+a[0][2]*b[2]+a[0][3]*b[3], 
    a[1][0]*b[0]+a[1][1]*b[1]+a[1][2]*b[2]+a[1][3]*b[3],
    a[2][0]*b[0]+a[2][1]*b[1]+a[2][2]*b[2]+a[2][3]*b[3], 
    a[3][0]*b[0]+a[3][1]*b[1]+a[3][2]*b[2]+a[3][3]*b[3]
  });
}
My3DPoint euclidian3DPoint (float[] a) {
  My3DPoint result = new My3DPoint(a[0]/a[3], a[1]/a[3], a[2]/a[3]);
  return result;
}
My3DBox transformBox(My3DBox box, float[][] transformMatrix) {
  My3DPoint[] p = box.p;
  My3DPoint[] s = new My3DPoint[]{euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[0]))),
                                  euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[1]))),
                                  euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[2]))),
                                  euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[3]))),
                                  euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[4]))),
                                  euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[5]))),
                                  euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[6]))),
                                  euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint(p[7]))),
                             };
  return new My3DBox(s);
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
       rX = rX + PI/16;
    } else if (keyCode == DOWN) {
        rX = rX - PI/16;
      } else if (keyCode == LEFT) {
          rY = rY + PI/16;
        } else if (keyCode == RIGHT) {
          rY = rY - PI/16;
        }
  }
    } 
    
void mouseDragged() 
{
   if (mouseY - pmouseY >0){
  scale = scale + 0.02;
   }else {
     scale = scale - 0.02;
  
  }
}

